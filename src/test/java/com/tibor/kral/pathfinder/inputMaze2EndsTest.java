package com.tibor.kral.pathfinder;

import org.junit.Assert;
import org.junit.Test;
import java.io.File;

public class inputMaze2EndsTest
{
    @Test
    public void inputMazeValidCheck()
    {
        Assert.assertEquals(false,MazeChecker.isMazeValid(new File("./src/test/resources/inputMaze2EndPoints.txt")));
    }
}