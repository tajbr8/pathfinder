package com.tibor.kral.pathfinder;

import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;

public class NonExistingPathTest
{
    @Test
    public void testPathExistance()
    {
        ArrayList<String> map = new ArrayList<String>();
        map.add("......#.............................");
        map.add("..S...#......................#######");
        map.add("......#......................#..X...");
        map.add("......#......................#######");
        map.add("......#........#....................");
        map.add("......#.......#.....................");
        map.add("..............#.....................");
        map.add("............#.......................");

        PathFinder pathFinder = new PathFinder(map);
        Assert.assertEquals("",String.join(",",pathFinder.getPath()));
    }
}
