package com.tibor.kral.pathfinder;

import org.junit.Assert;
import org.junit.Test;
import java.io.File;

public class inputMaze2StartsTest
{
    @Test
    public void inputMazeValidCheck()
    {
        Assert.assertEquals(false,MazeChecker.isMazeValid(new File("./src/test/resources/inputMaze2StartPoints.txt")));
    }
}
