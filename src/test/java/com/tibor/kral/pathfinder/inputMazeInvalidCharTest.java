package com.tibor.kral.pathfinder;

import org.junit.Assert;
import org.junit.Test;
import java.io.File;

public class inputMazeInvalidCharTest
{
    @Test
    public void inputMazeValidCheck()
    {
        Assert.assertEquals(false,MazeChecker.isMazeValid(new File("./src/test/resources/inputMazeInvalidCharacter.txt")));
    }
}
