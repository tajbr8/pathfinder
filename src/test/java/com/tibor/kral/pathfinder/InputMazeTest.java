package com.tibor.kral.pathfinder;

import org.junit.Assert;
import org.junit.Test;
import java.io.File;

public class InputMazeTest
{
    @Test
    public void inputMazeValidCheck()
    {
        Assert.assertEquals(true,MazeChecker.isMazeValid(new File("./src/test/resources/inputMazeCorrect.txt")));
    }
}