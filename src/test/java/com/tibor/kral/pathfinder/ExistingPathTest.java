package com.tibor.kral.pathfinder;

import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;

public class ExistingPathTest
{
    @Test
    public void testPathExistance()
    {
        ArrayList<String> map = new ArrayList<String>();
        map.add("......#.............................");
        map.add("..S...#......................#......");
        map.add("......#......................#..X...");
        map.add("......#......................#......");
        map.add("......#........#....................");
        map.add("......#.......#.....................");
        map.add("..............#.....................");
        map.add("............#.......................");
        PathFinder pathFinder = new PathFinder(map);
        Assert.assertEquals("d,r,r,r,d,d,d,d,r,r,u,u,u,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,d,r,r,u,u,r,r",String.join(",",pathFinder.getPath()));
    }
}