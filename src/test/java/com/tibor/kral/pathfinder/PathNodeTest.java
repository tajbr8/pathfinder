package com.tibor.kral.pathfinder;

import org.junit.Assert;
import org.junit.Test;

public class PathNodeTest
{
    @Test
    public void testFDistanceCalculation()
    {
        PathNode node = new PathNode(0,0,false);

        Assert.assertEquals(node.getfTotalDistance(),0);
        node.sethDistanceFromTarget(5);
        node.setgDistanceFromStart(5);
        Assert.assertEquals(node.getfTotalDistance(),0);
        node.recalculateFDistance();
        Assert.assertEquals(node.getfTotalDistance(),10);
    }
}
