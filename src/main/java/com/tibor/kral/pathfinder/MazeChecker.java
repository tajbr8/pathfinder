package com.tibor.kral.pathfinder;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MazeChecker {
    private static final Logger logger = Logger.getLogger(MazeChecker.class);

    /**
     *
     * @param inputMaze TXT File containing the input maze
     * @return true if all conditions are met:
     *         - the maze is a rectangle
     *         - there is only 1 start point and 1 end point
     *         - there are only valid nodes {. # S X}
     */
    public static boolean isMazeValid(File inputMaze) {
        int rows = 0;
        int cols = 0;
        boolean start = false;
        boolean finish = false;
        try(Scanner sc = new Scanner(inputMaze)) {
            while (sc.hasNext()) {
                String nextRow = sc.next();
                rows++;
                if (cols == 0) {
                    cols = nextRow.length();
                } else {
                    if (nextRow.length() != cols) {
                        logger.error(String.format("The map isn't a rectangle. Row %d has different length.", rows));
                        return false;
                    }
                }
                for(int i = 0; i<nextRow.length(); i++) {
                    String nextNode = Character.toString(nextRow.charAt(i));
                    if (nextNode.equalsIgnoreCase("s")) {
                        if (start == false) {
                            start = true;
                        }
                        else {
                            logger.error("The map contains more then 1 starting point.");
                            return false;
                        }
                    } else if (nextNode.equalsIgnoreCase("x")) {
                        if (finish == false) {
                            finish = true;
                        }
                        else {
                            logger.error("The map contains more then 1 target point.");
                            return false;
                        }
                    } else if (!nextNode.equalsIgnoreCase(".") && !nextNode.equalsIgnoreCase("#")) {
                        logger.error(String.format("Found invalid node: %s",nextNode));
                        return false;
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            logger.error(String.format("File %s could not be opened.",inputMaze));
            return false;
        }
        if (start == false) {
            logger.error("No starting point found.");
            return false;
        }
        if (finish == false) {
            logger.error("No target point found.");
            return false;
        }
        return true;
    }
}
