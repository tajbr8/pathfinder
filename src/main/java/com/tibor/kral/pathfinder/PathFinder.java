package com.tibor.kral.pathfinder;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class PathFinder {
    private PathNode[][] map;
    private PathNode startingNode;
    private PathNode targetNode;

    public PathFinder(List<String> maze) {
        this.map = new PathNode[maze.size()][maze.get(0).length()];
        this.createPathNodeMap(maze);
    }

    private void createPathNodeMap(List<String> maze) {
        int rowIndex = 0;
        for (String row : maze) {
            int colIndex = 0;
            for(int i = 0;i<row.length();i++) {
                String node = Character.toString(row.charAt(i));
                if (node.equalsIgnoreCase(".")) {
                    this.map[rowIndex][colIndex] = new PathNode(rowIndex,colIndex,false);
                } else if (node.equalsIgnoreCase("#")) {
                    this.map[rowIndex][colIndex] = new PathNode(rowIndex,colIndex,true);
                } else if (node.equalsIgnoreCase("s")) {
                    this.startingNode = new PathNode(rowIndex,colIndex,false);
                    this.map[rowIndex][colIndex] = this.startingNode;
                } else if (node.equalsIgnoreCase("x")) {
                    this.targetNode = new PathNode(rowIndex,colIndex,false);
                    this.map[rowIndex][colIndex] = this.targetNode;
                }
                colIndex++;
            }
            rowIndex++;
        }
        for (int tmpRow = 0; tmpRow<map.length;tmpRow++) {
            for (int tmpCol = 0;tmpCol<map[0].length;tmpCol++) {
                map[tmpRow][tmpCol].sethDistanceFromTarget(Math.abs(tmpRow-this.targetNode.getRow())+Math.abs(tmpCol-this.targetNode.getCol()));
            }
        }

    }

    /**
     *
     * @return List of steps leading from start to finish. Steps are letters:
     * - "u" - up
     * - "d" - down
     * - "r" - right
     * - "l" - left
     */
    public List<String> getPath() {
        LinkedList<String> foundPath = new LinkedList<>();
        LinkedList<PathNode> openedNodes = new LinkedList<PathNode>();
        LinkedList<PathNode> closedNodes = new LinkedList<PathNode>();;
        PathNodeComparator comparator = new PathNodeComparator();
        openedNodes.add(this.startingNode);
        int movementCost = 10;

        while (!openedNodes.isEmpty()) {
            Collections.sort(openedNodes,comparator);
            PathNode activeNode = openedNodes.pop();
            if (activeNode.equals(this.targetNode)) {

                // Target reached, lets backtrack the path

                PathNode currentNode = this.targetNode;
                while (currentNode.getPrevious() != null) {
                    PathNode prev = currentNode.getPrevious();
                    if (currentNode.getRow() > prev.getRow()) foundPath.addFirst("d");
                    else if (currentNode.getRow() < prev.getRow()) foundPath.addFirst("u");
                    else if (currentNode.getCol() > prev.getCol()) foundPath.addFirst("r");
                    else if (currentNode.getCol() < prev.getCol()) foundPath.addFirst("l");
                    currentNode = prev;
                }
                break;
            }

            activeNode.setClosed(true);
            closedNodes.add(activeNode);

            // Get neighbors that are not closed and not solid
            LinkedList<PathNode> neighbors = new LinkedList<PathNode>();
            if (activeNode.getRow() > 0) {
                PathNode topNeighbor = this.map[activeNode.getRow()-1][activeNode.getCol()];
                if (!topNeighbor.isClosed() && !topNeighbor.isSolid()) {
                    neighbors.add(topNeighbor);
                }
            }
            if (activeNode.getRow() < map.length-1) {
                PathNode botNeighbor = this.map[activeNode.getRow()+1][activeNode.getCol()];
                if (!botNeighbor.isClosed() && !botNeighbor.isSolid()) {
                    neighbors.add(botNeighbor);
                }
            }
            if (activeNode.getCol() > 0) {
                PathNode leftNeighbor = this.map[activeNode.getRow()][activeNode.getCol()-1];
                if (!leftNeighbor.isClosed() && !leftNeighbor.isSolid()) {
                    neighbors.add(leftNeighbor);
                }
            }
            if (activeNode.getCol() < map[0].length-1){
                PathNode rightNeighbor = this.map[activeNode.getRow()][activeNode.getCol()+1];
                if (!rightNeighbor.isClosed() && !rightNeighbor.isSolid()) {
                    neighbors.add(rightNeighbor);
                }
            }
            // Check neighbors, update their information and add to OpenedNodes list for future processing.
            for (PathNode neighbor : neighbors) {
                if (openedNodes.contains(neighbor)) {
                    int newG = activeNode.getgDistanceFromStart() + movementCost;
                    if (newG < neighbor.getgDistanceFromStart()) {
                        neighbor.setPrevious(activeNode);
                        neighbor.setgDistanceFromStart(newG);
                        neighbor.recalculateFDistance();
                    }
                } else {
                    neighbor.setgDistanceFromStart(activeNode.getgDistanceFromStart()+movementCost);
                    neighbor.setPrevious(activeNode);
                    neighbor.recalculateFDistance();
                    openedNodes.add(neighbor);
                }
            }
        }
        return foundPath;
    }
}
