package com.tibor.kral.pathfinder;

import org.apache.log4j.Logger;
import java.io.File;

public class App 
{
    private static final Logger logger = Logger.getLogger(App.class);
    public static void main( String[] args ) {
        if (args.length == 0) {
            System.err.println("Please enter name of file containing the maze in the attribute.");
            return;
        }
        File inputFile = new File(args[0]);
        if (MazeChecker.isMazeValid(inputFile)) {
            PathFinder pathFinder = new PathFinder(MazeConverter.convertFileToList(inputFile));
            System.out.println("output:"+String.join(",",pathFinder.getPath()));
        }
    }
}
