package com.tibor.kral.pathfinder;

public class PathNode {
    private int fTotalDistance;
    private int gDistanceFromStart;
    private int hDistanceFromTarget;
    private boolean solid;
    private PathNode previous;
    private boolean closed;
    private int row;
    private int col;

    public PathNode(int row, int col, boolean solid) {
        this.row = row;
        this.col = col;
        this.solid = solid;
        this.closed = false;
        this.hDistanceFromTarget = 0;
        this.gDistanceFromStart = 0;
        this.fTotalDistance = 0;
        this.previous = null;
    }

    public void setgDistanceFromStart(int gDistanceFromStart) {
        this.gDistanceFromStart = gDistanceFromStart;
    }

    public void sethDistanceFromTarget(int hDistanceFromTarget) {
        this.hDistanceFromTarget = hDistanceFromTarget;
    }

    public int getfTotalDistance() {
        return fTotalDistance;
    }

    public int getgDistanceFromStart() {
        return gDistanceFromStart;
    }

    public boolean isSolid() {
        return solid;
    }

    public PathNode getPrevious() {
        return previous;
    }

    public boolean isClosed() {
        return closed;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public void setPrevious(PathNode previous) {
        this.previous = previous;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public void recalculateFDistance() {
        this.fTotalDistance = this.gDistanceFromStart + this.hDistanceFromTarget;
    }


}
