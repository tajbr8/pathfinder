package com.tibor.kral.pathfinder;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MazeConverter {
    private static final Logger logger = Logger.getLogger(MazeConverter.class);

    public static List<String> convertFileToList(File inputFile) {
        ArrayList<String> rows = new ArrayList<>();
        try(Scanner sc = new Scanner(inputFile)) {
            while (sc.hasNextLine()) {
                rows.add(sc.nextLine());
            }
        } catch (FileNotFoundException e) {
            logger.error("File could not be opened");
            return null;
        }
        return rows;
    }
}
