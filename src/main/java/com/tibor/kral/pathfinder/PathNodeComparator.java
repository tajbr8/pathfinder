package com.tibor.kral.pathfinder;

import java.util.Comparator;

public class PathNodeComparator implements Comparator<PathNode> {

    @Override
    public int compare(PathNode node1, PathNode node2) {
        return Integer.compare(node1.getfTotalDistance(),node2.getfTotalDistance());
    }
}
